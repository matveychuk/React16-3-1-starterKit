import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import IndexPage from 'app/components/IndexPage';
import style from './style.scss';

class App extends React.Component {

	static childContextTypes = {
		inf: PropTypes.string
	};

	getChildContext() {
		return {
			inf: 'hello from context'
		};
	}

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<BrowserRouter>
				<div className={style.wrapper}>
					<Switch>
						<Route exact path='/' render={(props) => (
							<IndexPage {...props} />
						)} />
					</Switch>
				</div>
			</BrowserRouter>
		);
	}
}

export default (App);
