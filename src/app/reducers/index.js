import { combineReducers } from 'redux';
import stats from 'app/ducks/stats';

export default combineReducers({
	stats
});
