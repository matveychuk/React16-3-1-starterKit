import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import style from './style.scss';
import { selectors as statsSelectors, updateSomeData } from 'app/ducks/stats';

class IndexPage extends Component {

	static contextTypes = {
		inf: PropTypes.string
	};

	handleClick = () => {
		const { dispatch, someData } = this.props;
		dispatch(updateSomeData(someData + 1));
	}

	render() {
		const { inf } = this.context;
		const { someData } = this.props;
		return (
			<div className={style.IndexPage} >
				<h2>{inf}</h2>
				<p>{someData}</p>
				<button onClick={this.handleClick}>click</button>
			</div>
		);
	}
}

IndexPage.propTypes = {
	dispatch: PropTypes.func
	, someData: PropTypes.number
};

const mapStateToProps = (state) => {

	return {
		someData: statsSelectors.getSomeData(state)
	};
};

export default (connect(mapStateToProps)(IndexPage));
