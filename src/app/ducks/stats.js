export const SET_SOME_DATA = 'SET_SOME_DATA';

const initialState = {
	someData: 0

};

export default (state = initialState, { type, payload }) => {
	switch (type) {

		case SET_SOME_DATA:
			return ({
				...state, someData: payload
			});

		default:
			return state;
	}
};

export const setSomeData = (payload) => (
	{
		type: SET_SOME_DATA
		, payload
	}
);

export const updateSomeData = numClick => dispatch => {
	dispatch(setSomeData(numClick));

};

export const selectors = {
	getSomeData: (state) => state.stats.someData
};
