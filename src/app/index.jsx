import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from 'app/store';
import 'app/main.scss';
import App from 'app/pages';

render(
	<Provider store={store}>
		<App />
	</Provider>
	, document.querySelector('#rm-root')
);

