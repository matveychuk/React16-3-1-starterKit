const cssLoader = require('./css');

module.exports = ({ isProd, sourcePath }) => {

	const cssLoaderModules = cssLoader({ isProd, sourcePath });
	cssLoaderModules[0].options.module = true;
	return cssLoaderModules;
};
