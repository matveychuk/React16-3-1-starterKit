const webpack = require('webpack');

module.exports = ({ nodeEnv }) => ([
	new webpack.DefinePlugin({
		'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
		, 'process.env.NODE_ENV': JSON.stringify(nodeEnv)
	})
]);

