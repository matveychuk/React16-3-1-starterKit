const webpack = require('webpack');

module.exports = () => ([
	new webpack.optimize.CommonsChunkPlugin({
		name: 'common'
		, async: true
		, children: true
		, minChunks: 2
	})
]);
