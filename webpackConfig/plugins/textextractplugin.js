const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = ({ isDevServ }) => {
	return isDevServ
		? [new ExtractTextPlugin({ filename: 'styles.css', allChunks: true })]
		: [new ExtractTextPlugin({ filename: 'styles.css', allChunks: true })];
};
