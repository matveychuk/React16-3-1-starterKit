const webpack = require('webpack');


module.exports = ({ isDevServ }) => {
	return isDevServ
		? [new webpack.HotModuleReplacementPlugin()]
		: [];
};
