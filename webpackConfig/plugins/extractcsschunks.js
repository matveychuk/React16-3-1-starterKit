const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');

module.exports = ({ isDevServ }) => {
	return isDevServ
		? [new ExtractCssChunks()]
		: [new ExtractCssChunks()];
};
