const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = ({ isProd }) => {
	return isProd
		? [
			new UglifyJsPlugin({
				sourceMap: false
				, parallel: true
			})
		]
		: [];
};

