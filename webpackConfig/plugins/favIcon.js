const fs = require('fs');
const path = require('path');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = ({ sourcePath, isProd }) => {

	const faviconPath = path.resolve(sourcePath, './favicon.png');

	if (isProd && fs.existsSync(faviconPath)) {
		return ([
			new FaviconsWebpackPlugin({
				prefix: 'assets/favicon/', logo: 'favicon.png'
			})
		]);
	}
	return ([]);
};





