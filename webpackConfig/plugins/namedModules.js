const webpack = require('webpack');


module.exports = ({ isProd }) => {
	return isProd
		? []
		: [new webpack.NamedModulesPlugin()];
};
