module.exports = ({ exclude }) => {
	return ({
		test: /\.(png|jpg|jpeg|gif|tiff|webp|ico)$/
		, exclude
		, use: [
			{
				loader: 'url-loader'
				, options: {
					limit: 8192
					, name: 'assets/[hash:6]/[hash].[ext]'
					, fallback: 'file-loader'
				}
			}
		]
	});
};
