module.exports = ({ include, exclude }) => {

	return {
		test: /\.jsx?$/
		, include
		, exclude
		, loader: 'babel-loader'
		, options: {
			cacheDirectory: true
		}
	};

};
