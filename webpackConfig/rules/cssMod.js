const cssModLoader = require('../loaders/cssMod');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = ({ exclude, isProd, sourcePath }) => {
	return ({
		test: /\.(scss|sass|css)$/
		, exclude
		, use: ExtractTextPlugin.extract({
			fallback: 'style-loader'
			, use: cssModLoader({ isProd, sourcePath })
		})
	});
};
