const path = require('path');

//rules
const javascript = require('./webpackConfig/rules/javascript');
const cssMod = require('./webpackConfig/rules/cssMod');
const css = require('./webpackConfig/rules/css');
const fonts = require('./webpackConfig/rules/fonts');
const html = require('./webpackConfig/rules/html');
const images = require('./webpackConfig/rules/images');

//plugins
const bunndleAnalyzer = require('./webpackConfig/plugins/bundleAnalyzer');
const commonChunk = require('./webpackConfig/plugins/commonChunk');
const compression = require('./webpackConfig/plugins/compression');
const contextReplacement = require('./webpackConfig/plugins/contextReplacement');
const define = require('./webpackConfig/plugins/define');
const favIcon = require('./webpackConfig/plugins/favIcon');
const hmr = require('./webpackConfig/plugins/hmr');
const htmlWebpack = require('./webpackConfig/plugins/htmlWebpack');
const imageMin = require('./webpackConfig/plugins/imageMin');
const namedModules = require('./webpackConfig/plugins/namedModules');
const noEmitsOnErrors = require('./webpackConfig/plugins/noEmitsOnErrors');
const preload = require('./webpackConfig/plugins/preload');
const uglifyJs = require('./webpackConfig/plugins/uglifyJs');
const textExtract = require('./webpackConfig/plugins/textextractplugin');

module.exports = function (env) {

	const stats = {
		assets: true
		, children: false
		, chunks: false
		, hash: false
		, modules: false
		, publicPath: false
		, timings: true
		, version: false
		, warnings: true
		, colors: {
			green: '\u001b[32m'
		}
	};

	const nodeEnv = process.env.NODE_ENV || 'production';

	const isTest = env.test === 'true';
	const isProd = nodeEnv === 'production';
	const isDevServ = env.dev_serv === 'true';
	const sourcePath = path.join(__dirname, './src');

	const buildPath = env.output_dir ? path.resolve(__dirname, env.output_dir) : path.resolve(__dirname, './build');

	const host = env.host || '0.0.0.0';
	const port = env.port || 3000;
	const proxyHost = env.proxy_host || '0.0.0.0';
	const proxyPort = env.proxy_port || 4000;
	const watch = !!((env && env.watch === 'true'));
	const entryPoint = isTest ? undefined : ['babel-polyfill', './app'];
	if (isDevServ) {
		entryPoint.unshift(
			'react-hot-loader/patch'
			, `webpack-dev-server/client?http://${host}:${port}`
			, 'webpack/hot/only-dev-server'
		);
	}

	const config = {
		isTest
		, isProd
		, isDevServ
		, sourcePath
		, nodeEnv
	};

	return { // 'inline-source-map' -- for karma
		devtool: isProd ? false : 'cheap-module-source-map'
		, context: sourcePath
		, entry: isTest ? undefined : entryPoint
		, output: {
			path: buildPath
			, publicPath: '/'
			, filename: '[name].js'
			, chunkFilename: '[name]-[chunkhash:8].js',
		}
		, module: {
			rules: [
				fonts({ ...config })
				, cssMod({ ...config, exclude: [/node_modules/, /.glob.scss$/] })
				, css({ ...config, include: [/node_modules/, /.glob.scss$/] })
				, javascript({ ...config, exclude: [/node_modules/] })
				, html({ ...config })
				, images({ ...config, exclude: /node_modules/ })
			]
		}
		, plugins: [
			...bunndleAnalyzer(config)
			, ...commonChunk(config)
			, ...compression(config)
			, ...contextReplacement(config)
			, ...define(config)
			, ...favIcon(config)
			, ...hmr(config)
			, ...imageMin(config)
			, ...namedModules(config)
			, ...noEmitsOnErrors(config)
			, ...preload(config)
			, ...uglifyJs(config)
			, ...textExtract(config)
			, ...htmlWebpack(config)
		]
		, resolve: {
			extensions: ['.js', '.jsx']
			, modules: ['node_modules', sourcePath]
		}
		, target: 'web'
		, performance: isProd && {
			maxAssetSize: 1024 * 400
			, maxEntrypointSize: 1024 * 500
			, hints: 'warning'
		}
		, stats
		, watch
		, watchOptions: {
			aggregateTimeout: 300
			, poll: 1000
		}
		, devServer: {
			contentBase: sourcePath
			, publicPath: '/'
			, historyApiFallback: true
			, port
			, host
			, hot: !isProd
			, compress: isProd
			, stats
			, noInfo: false
			, proxy: {
				'/socket.io/': {
					target: `http://${proxyHost}:${proxyPort}`
					, ws: true,
				}
				, '/api/': `http://${proxyHost}:${proxyPort}`
				, '/img/': `http://${proxyHost}:${proxyPort}`
				, '/upload/': `http://${proxyHost}:${proxyPort}`
			}
		}
	};
};
